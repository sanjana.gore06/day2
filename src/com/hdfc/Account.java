package com.hdfc;

import java.util.Date;

public abstract class Account {
	private static String accNo;
	private static String name;
	
	
	//constructors
	
	public Account()
	{
		System.out.println("Account created at: "+ new Date());
	}
	public Account(String accNo,String name,float roi)
	{
		super();
		Account.accNo=accNo;
		Account.name=name;
		
	}
	public String getAccNo() {
		return accNo;
	}
	//not relevant
	public void setAccNo(String accNo) {
		Account.accNo = accNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		Account.name = name;
	}

	@Override
	public String toString() {
		return "Account [accNo=" + accNo + ", name=" + name + "]";
	}
}
