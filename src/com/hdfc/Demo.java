package com.hdfc;

import java.time.LocalDate;

public class Demo {

	public static void main(String[] args) {
		System.out.println("Welcome to Banking");
		System.out.println("-------------------------");
		// Account a1 = new Account(); //will create empty account object
		// TODO Auto-generated method stub
		// Account a2 = new Account("AC-5768","dm9897",0.0f);
		// System.out.println(a1.getAccNo() + " " +a1.getName() + " " +a1.getRoi());
		// a1.setAccNo("A1-101");
		// a1.setName("Sanjana");
		// System.out.println(a2.getAccNo() + " " +a2.getName() + " " +a2.getRoi());
		//

		// System.out.println(a1.toString());
		// System.out.println(a2.toString());
		//
		Account sa = new SavingAccount("SA-201", "dm201", 5.6f, 12000, "supersavingaccount");
		System.out.println(sa);

		Account cc = new Creditcard("CC-434", "vsdgc", 24.5f, "223 463473", LocalDate.of(2022, 12, 10),
				LocalDate.of(2022, 12, 01));
		System.out.println(cc);

	}

}
