package com.hdfc;
import java.time.LocalDate;
public class Creditcard extends Account
{
    private String ccnumber;
    private LocalDate duedate;
    private LocalDate statementDate;
    private static float roi;
    
    
	

	public static float getRoi() {
		return roi;
	}

	public static void setRoi(float roi) {
		Creditcard.roi = roi;
	}

	public Creditcard(String accNo, String name, float roi, String ccnumber, LocalDate duedate,
			LocalDate statementDate) {
		super(accNo, name, roi);
		this.ccnumber = ccnumber;
		this.duedate = duedate;
		this.statementDate = statementDate;
	}

	public String getCcnumber() {
		return ccnumber;
	}

	public void setCcnumber(String ccnumber) {
		this.ccnumber = ccnumber;
	}

	public LocalDate getDuedate() {
		return duedate;
	}

	public void setDuedate(LocalDate duedate) {
		this.duedate = duedate;
	}

	public LocalDate getStatementDate() {
		return statementDate;
	}

	public void setStatementDate(LocalDate statementDate) {
		this.statementDate = statementDate;
	}

	@Override
	public String toString() {
		return "Creditcard [ccnumber=" + ccnumber + ", duedate=" + duedate + ", statementDate=" + statementDate
				+ ", getAccNo()=" + getAccNo() + ", getName()=" + getName() + ", getRoi()=" + getRoi() + "]";
	}

	 
	void getInterest() {
		// TODO Auto-generated method stub
		
	}

	
	
	

}
